// exo 1
const getExoUn = () => {
  const array = [1, 2, 3, 4];
  return array.reduce((iterator, currentValue) => iterator + currentValue);
};

// exo 2
const getExoDeux = () => {
  const array = [
    { id: 1, type: "house" },
    { id: 2, type: "house" },
    { id: 3, type: "destination" },
    { id: 4, type: "house" },
    { id: 5, type: "destination" },
  ];
  return array.filter((element) => element.type === "house");
};

// exo 3
const getExoTrois = () => {
  const array = [
    { id: 1, type: "house" },
    { id: 2, type: "house" },
    { id: 3, type: "destination" },
    { id: 4, type: "house" },
    { id: 5, type: "destination" },
  ];
  return array.find((element) => element.id === 2);
};
