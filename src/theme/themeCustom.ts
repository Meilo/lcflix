export interface ThemeT {
  padding: {
    tiny: number;
    verySmall: number;
    small: number;
    standard: number;
    middle: number;
    large: number;
    veryLarge: number;
    extraLarge: number;
  };
  fontSize: {
    small: number;
    standard: number;
    large: number;
  };
  color: {
    text: string;
    white: string;
    darkMode: string;
    error: string;
    primary: string;
  };
}

const themeCustom: ThemeT = {
  padding: {
    tiny: 2,
    verySmall: 5,
    small: 10,
    standard: 20,
    middle: 25,
    large: 30,
    veryLarge: 50,
    extraLarge: 70,
  },
  fontSize: {
    small: 10,
    standard: 15,
    large: 25,
  },
  color: {
    text: "#000000",
    white: "#ffffff",
    darkMode: "#222B45",
    error: "#DB2C66",
    primary: "#FFB101",
  },
};

export default themeCustom;
