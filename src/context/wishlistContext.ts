import * as React from "react";

const Wishlist: React.Context<any> = React.createContext({});

const WishlistProvider = Wishlist.Provider;
const WishlistConsumer = Wishlist.Consumer;

export { WishlistConsumer, WishlistProvider };
