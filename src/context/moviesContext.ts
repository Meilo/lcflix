import * as React from "react";

const Movies: React.Context<any> = React.createContext({});

const MoviesProvider = Movies.Provider;
const MoviesConsumer = Movies.Consumer;

export { MoviesConsumer, MoviesProvider };
