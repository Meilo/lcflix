import React from "react";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { CommonActions } from "@react-navigation/native";
import { Dashboard, Wishlist } from "./movies";
import { TabView, Tab, Text } from "@ui-kitten/components";
import { Icon, Flex } from "./base-components";
import { WishlistConsumer } from "./context/wishlistContext";

const { Navigator, Screen } = createBottomTabNavigator();

interface Props {
  navigation: {
    dispatch: (data: any) => void;
  };
  state: {
    index: number;
    routeNames: string;
  };
}

interface IconProps {
  props: Readonly<Object> | undefined;
}

const DashboardIcon = (Props: IconProps["props"]) => (
  <Icon {...Props} name="home-outline" />
);
const WishlistIcon = (Props: IconProps["props"]) => {
  return (
    <WishlistConsumer>
      {({ wishlist }) => {
        if (!wishlist) return null;
        return (
          <Flex>
            <Icon {...Props} name="list-outline" />
            <Text>{wishlist.items.length}</Text>
          </Flex>
        );
      }}
    </WishlistConsumer>
  );
};

const TopTabBar = ({ navigation, state }: Props) => {
  const shouldLoadComponent = (index: number) => index === state.index;
  return (
    <TabView
      selectedIndex={state.index}
      onSelect={(index) =>
        navigation.dispatch(
          CommonActions.navigate({
            name: state.routeNames[index],
          })
        )
      }
      shouldLoadComponent={shouldLoadComponent}
      tabBarStyle={{
        height: 80,
      }}
    >
      <Tab icon={DashboardIcon} />
      <Tab icon={WishlistIcon} />
    </TabView>
  );
};

const MoviesNavigation = () => {
  return (
    <Navigator
      tabBar={({ navigation, state }) => (
        <TopTabBar navigation={navigation} state={state} />
      )}
    >
      <Screen name="Dashboard" component={Dashboard} />
      <Screen name="Wishlist" component={Wishlist} />
    </Navigator>
  );
};

export default MoviesNavigation;
