export const ellipsisText = (input: string) => {
  if (input.length >= 10) {
    return input.substring(0, 15) + "...";
  }
  return input;
};
