import React from "react";
import styled from "styled-components";
import { WishlistConsumer } from "../context/wishlistContext";
import { Button as RawButton } from "@ui-kitten/components";

interface Props {
  isInWishlist: boolean;
  refreshing: Array<(movieId: string) => void>;
  movieId: string;
  onlyRemoveButton?: boolean;
}

const Button = styled(RawButton)`
  background-color: ${({ isInWishlist }: { isInWishlist: boolean }) =>
    isInWishlist ? `#DB2C66` : `#FFB101`};
  border-color: ${({ isInWishlist }: { isInWishlist: boolean }) =>
    isInWishlist ? `#DB2C66` : `#FFB101`};
`;

const WishlistButton = ({
  isInWishlist,
  refreshing,
  movieId,
  onlyRemoveButton,
}: Props) => {
  const dataToSend = { body: { media_id: movieId } };
  return (
    <WishlistConsumer>
      {({ addMovie, refreshAllMovies, removeMovie }) => (
        <Button
          isInWishlist={isInWishlist || onlyRemoveButton}
          onPress={async () => {
            if (isInWishlist || onlyRemoveButton) await removeMovie(dataToSend);
            else await addMovie(dataToSend);
            await refreshAllMovies();
            await Promise.all(refreshing.map((item) => item(movieId)));
          }}
        >
          {isInWishlist || onlyRemoveButton ? `Remove` : `Add`}
        </Button>
      )}
    </WishlistConsumer>
  );
};

export default WishlistButton;
