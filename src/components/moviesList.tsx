import React from "react";
import styled from "styled-components";
import { FlatList, Image, TouchableOpacity } from "react-native";

import { Text, Button } from "@ui-kitten/components";
import { Flex, Padded } from "../base-components";
import { ellipsisText } from "./helpers";
import WishlistButton from "./wishlistButton";
import { MoviesConsumer } from "../context/moviesContext";

const Container = styled(TouchableOpacity)`
  width: 160px;
  height: 250px;
  align-items: center;
  padding-top: 10px;
`;

const ImageMovie = styled(Image)`
  width: 150px;
  height: 250px;
  border-radius: 20px;
`;

interface Props {
  navigation: {
    push: (route: string, params: Object) => void;
  };
  data: Array<{
    id: string;
    title: string;
    poster: string;
    isInWishlist: boolean;
  }>;
  dataUpdater?: (movieId: string) => void;
  onlyRemoveButton?: boolean;
}

const MoviesList = ({
  data,
  navigation,
  onlyRemoveButton = false,
  dataUpdater,
}: Props) => {
  const snapToInterval = 150;
  const decelerationRate = 0;

  return (
    <MoviesConsumer>
      {({ refreshAllMovies }) => (
        <FlatList
          showsHorizontalScrollIndicator={false}
          data={data}
          horizontal
          keyExtractor={(item) => item.id}
          contentContainerStyle={{
            alignItems: "center",
          }}
          snapToInterval={snapToInterval}
          decelerationRate={decelerationRate}
          renderItem={({ item }) => {
            return (
              <Flex column>
                <Container
                  onPress={() => navigation.push("Details", { id: item.id })}
                >
                  <ImageMovie source={{ uri: item.poster }} />
                </Container>
                <Padded top left size="small">
                  <Text>{ellipsisText(item.title)}</Text>
                  <Padded top size="small" />
                  <WishlistButton
                    isInWishlist={item.isInWishlist}
                    refreshing={
                      dataUpdater
                        ? [dataUpdater, refreshAllMovies]
                        : [refreshAllMovies]
                    }
                    movieId={item.id}
                    onlyRemoveButton={onlyRemoveButton}
                  />
                </Padded>
              </Flex>
            );
          }}
        />
      )}
    </MoviesConsumer>
  );
};

export default MoviesList;
