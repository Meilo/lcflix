import React from "react";
import styled from "styled-components";
import { View, ImageBackground, TouchableOpacity } from "react-native";
import { Text } from "@ui-kitten/components";
import { Padded } from "../base-components";
import { useOneMovie } from "../api";

const DEFAULT_WIDTH_SIZE = 100;
const DEFAULT_HEIGHT_SIZE = 60;

const Container = styled(TouchableOpacity)`
  width: ${DEFAULT_WIDTH_SIZE}%;
  height: ${DEFAULT_HEIGHT_SIZE}%;
`;

const ImageBackgroundMovie = styled(ImageBackground)`
  width: ${DEFAULT_WIDTH_SIZE}%;
  height: ${DEFAULT_WIDTH_SIZE}%;
`;

const TextContainer = styled(View)`
  margin-top: 450px;
`;

const Title = styled(Text)`
  color: white;
  font-size: 30px;
  font-weight: bold;
`;

interface Props {
  movieId: number;
  navigation: any;
}

const TopViewMovie = ({ movieId, navigation }: Props) => {
  const { movie, isLoading } = useOneMovie({ movieId });
  if (isLoading) return null;

  return (
    <Container onPress={() => navigation.push("Details", { id: movie.id })}>
      <ImageBackgroundMovie source={{ uri: movie.poster }} resizeMode="cover">
        <TextContainer>
          <Padded left>
            <Title>{movie.title}</Title>
          </Padded>
        </TextContainer>
      </ImageBackgroundMovie>
    </Container>
  );
};

export default TopViewMovie;
