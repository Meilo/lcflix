import React from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import App from "./app";
import themeCustom from "./theme/themeCustom";
import Details from "./movies/details";

const { Navigator, Screen } = createStackNavigator();

interface Props {
  apparence: string;
}

const AppNavigator = ({ apparence }: Props) => {
  const {
    color: { darkMode, white, text },
  } = themeCustom;
  const backgroundColorMode = apparence === "light" ? white : darkMode;
  const colorMode = apparence === "light" ? text : white;
  const headerCustomStyle = {
    headerStyle: {
      backgroundColor: backgroundColorMode,
      elevation: 0,
      shadowOpacity: 0,
      borderBottomWidth: 0,
    },
    headerBackTitleStyle: {
      color: colorMode,
    },
    headerTitleStyle: {
      color: colorMode,
    },
  };
  const themeNavigation = {
    dark: apparence === "dark",
    colors: { background: backgroundColorMode },
  };
  return (
    <NavigationContainer theme={themeNavigation}>
      <Navigator headerMode="screen">
        <Screen
          name="App"
          component={App}
          options={{
            headerShown: false,
            ...headerCustomStyle,
          }}
        />
        <Screen
          name="Details"
          component={Details}
          options={{
            title: "Details",
            headerBackTitle: "Back",
            ...headerCustomStyle,
          }}
        />
      </Navigator>
    </NavigationContainer>
  );
};

export default AppNavigator;
