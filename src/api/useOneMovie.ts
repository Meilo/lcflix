import { useState, useEffect } from "react";
import {
  getImagePath,
  getOneMovieApiUrl,
  getItemStatusApiUrl,
} from "./helpers";

const useOneMovie = ({ movieId }: { movieId: number }) => {
  const [movie, setMovie] = useState<any>(null);
  const [isInWishlist, setIsInWishlist] = useState(false);
  const [isLoading, setLoading] = useState(true);
  const [errors, setErrors] = useState(null);

  const API_URL = getOneMovieApiUrl(movieId);

  const getItemStatus = async (movieId: string) => {
    try {
      const statusItem = await fetch(getItemStatusApiUrl(movieId)).then(
        (data) => data.json()
      );
      setIsInWishlist(statusItem.item_present);
    } catch (error) {
      setErrors(error);
    }
  };

  const getOneMovie = async () => {
    try {
      setLoading(true);
      const results = await fetch(API_URL).then((data) => data.json());
      await getItemStatus(String(movieId));
      const newMovie = {
        id: String(results.id),
        title: results.title,
        poster: results.belongs_to_collection
          ? getImagePath(results.belongs_to_collection.poster_path)
          : getImagePath(results.backdrop_path),
        overview: results.overview,
      };
      setMovie(newMovie);
    } catch (error) {
      setErrors(error);
    } finally {
      setLoading(false);
    }
  };

  useEffect(() => {
    getOneMovie();
  }, []);

  return {
    movie,
    isLoading,
    errors,
    refreshMovie: getOneMovie,
    isInWishlist,
    refreshIsInWhislist: getItemStatus,
  };
};

export default useOneMovie;
