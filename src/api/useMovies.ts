import { useState, useEffect } from "react";
import { getImagePath, DISCOVER_MOVIE_API_URL, getItemStatus } from "./helpers";

const useMovies = () => {
  const [movies, setMovies] = useState<any>(null);
  const [isLoading, setLoading] = useState(true);
  const [errors, setErrors] = useState(null);

  const getAllMovies = async () => {
    try {
      setLoading(true);
      const { results } = await fetch(DISCOVER_MOVIE_API_URL).then((data) =>
        data.json()
      );
      const newMovies = await Promise.all(
        results.slice(0, 5).map(async (movie: any) => {
          const itemStatus = await getItemStatus(String(movie.id));
          return {
            id: String(movie.id),
            title: movie.title,
            poster: getImagePath(movie.poster_path),
            isInWishlist: itemStatus.item_present,
          };
        })
      );
      setMovies(newMovies);
    } catch (error) {
      setErrors(error);
    } finally {
      setLoading(false);
    }
  };

  useEffect(() => {
    getAllMovies();
  }, []);

  return { movies, isLoading, errors, refreshAllMovies: getAllMovies };
};

export default useMovies;
