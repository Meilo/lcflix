import { useState, useEffect } from "react";

import {
  getImagePath,
  LIST_API_URL,
  ADD_ITEM_API_URL,
  getItemStatus,
  REMOVE_ITEM_API_URL,
} from "./helpers";

const init = (body: any) => ({
  method: "POST",
  headers: {
    Accept: "application/json",
    "Content-Type": "application/json",
  },
  body: JSON.stringify(body),
});

const useWishlist = () => {
  const [wishlist, setWishlist] = useState<any>(null);
  const [isLoading, setLoading] = useState(true);
  const [errors, setErrors] = useState(null);

  const getAllMovies = async () => {
    try {
      setLoading(true);
      const results = await fetch(LIST_API_URL).then((data) => data.json());
      const newWishlist = {
        id: String(results.id),
        name: results.name,
        items: results.items.map((movie: any) => ({
          id: String(movie.id),
          title: movie.name ? movie.name : movie.title,
          poster: getImagePath(movie.poster_path),
        })),
      };
      setWishlist(newWishlist);
    } catch (error) {
      setErrors(error);
    } finally {
      setLoading(false);
    }
  };

  const addMovieInWishlist = async ({ body }: { body: any }) => {
    try {
      const response = await getItemStatus(body.media_id);
      if (!response.item_present)
        await fetch(ADD_ITEM_API_URL, init(body)).then((data) => data.json());
      else return "Already in list";
    } catch (error) {
      setErrors(error);
    }
  };

  const removeMovieInWishlist = async ({ body }: { body: any }) => {
    try {
      const itemStatus = await getItemStatus(body.media_id);
      if (itemStatus.item_present)
        await fetch(REMOVE_ITEM_API_URL, init(body)).then((data) =>
          data.json()
        );
      else return "Is not in list";
    } catch (error) {
      setErrors(error);
    }
  };

  useEffect(() => {
    getAllMovies();
  }, []);

  return {
    wishlist,
    isLoading,
    errors,
    refreshAllMovies: getAllMovies,
    addMovie: addMovieInWishlist,
    removeMovie: removeMovieInWishlist,
  };
};

export default useWishlist;
