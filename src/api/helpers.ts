const API_URL = `https://api.themoviedb.org/3`;
const API_KEY = `9843c2a8c1f4a172aae7abba02f3cfd2`;
const SESSION_ID = `1e243d78702ddb4c08f33cb77ea4cf8555a516c4`;

export const LIST_API_URL = `${API_URL}/list/7103256?api_key=${API_KEY}&language=en-US`;
export const ADD_ITEM_API_URL = `${API_URL}/list/7103256/add_item?api_key=${API_KEY}&session_id=${SESSION_ID}`;
export const DISCOVER_MOVIE_API_URL = `${API_URL}/discover/movie/?api_key=${API_KEY}`;
export const REMOVE_ITEM_API_URL = `${API_URL}/list/7103256/remove_item?api_key=${API_KEY}&session_id=${SESSION_ID}`;

export const getImagePath = (path: string) =>
  `https://image.tmdb.org/t/p/w440_and_h660_face${path}`;

export const getItemStatusApiUrl = (id: string) =>
  `${API_URL}/list/7103256/item_status?api_key=${API_KEY}&movie_id=${id}`;

export const getOneMovieApiUrl = (movieId: number) =>
  `${API_URL}/movie/${String(movieId)}?api_key=${API_KEY}`;

export const getItemStatus = async (movieId: string) =>
  await fetch(getItemStatusApiUrl(movieId)).then((data) => data.json());
