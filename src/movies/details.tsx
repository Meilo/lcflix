import React from "react";
import { ScrollView, Image } from "react-native";
import { Text } from "@ui-kitten/components";
import { Padded } from "../base-components";
import { useOneMovie } from "../api";
import { WishlistButton } from "../components";
import { MoviesConsumer } from "../context/moviesContext";

const Details = ({ route }: { route: { params: { id: string } } }) => {
  const movieId = Number(route.params.id);
  const { movie, isLoading, errors, isInWishlist, refreshIsInWhislist } =
    useOneMovie({ movieId });

  if (isLoading || errors) return null;

  return (
    <ScrollView
      contentContainerStyle={{
        flex: 1,
      }}
    >
      <Image
        source={{ uri: movie.poster }}
        style={{
          width: "100%",
          height: "60%",
        }}
      />
      <Padded left top right>
        <Text style={{ fontSize: 25, fontWeight: "bold" }}>{movie.title}</Text>
        <Text>{movie.overview}</Text>
        <Padded top>
          <MoviesConsumer>
            {({ refreshAllMovies }) => (
              <WishlistButton
                isInWishlist={isInWishlist}
                refreshing={[refreshIsInWhislist, refreshAllMovies]}
                movieId={movie.id}
              />
            )}
          </MoviesConsumer>
        </Padded>
      </Padded>
    </ScrollView>
  );
};

export default Details;
