import React from "react";
import { ScrollView } from "react-native";
import { WishlistConsumer } from "../context/wishlistContext";
import { MoviesList } from "../components";

const Wishlist = ({
  navigation,
}: {
  navigation: {
    push: (route: string, params: Object) => void;
  };
}) => (
  <ScrollView
    contentContainerStyle={{
      flex: 1,
      alignItems: "center",
      justifyContent: "center",
    }}
  >
    <WishlistConsumer>
      {({ wishlist, refreshAllMovies }) => (
        <MoviesList
          data={wishlist.items}
          navigation={navigation}
          dataUpdater={refreshAllMovies}
          onlyRemoveButton
        />
      )}
    </WishlistConsumer>
  </ScrollView>
);

export default Wishlist;
