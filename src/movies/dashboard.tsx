import React from "react";
import { ScrollView } from "react-native";
import { MoviesList, TopViewMovies } from "../components";
import { MoviesConsumer } from "../context/moviesContext";

const Dashboard = ({
  navigation,
}: {
  navigation: {
    push: (route: string, params: Object) => void;
  };
}) => (
  <ScrollView
    contentContainerStyle={{
      flex: 1,
      alignItems: "center",
      justifyContent: "center",
    }}
  >
    <TopViewMovies navigation={navigation} movieId={51550} />
    <MoviesConsumer>
      {({ movies }) => {
        return <MoviesList data={movies} navigation={navigation} />;
      }}
    </MoviesConsumer>
  </ScrollView>
);

export default Dashboard;
