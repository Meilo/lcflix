import styled from "styled-components";
import { View } from "react-native";

const Flex = styled(View)`
  display: flex;
  flex-direction: ${({ column }: { column: boolean }) =>
    column ? `column` : `row`};
  justify-content: ${({ justifyContent }: { justifyContent: string }) =>
    justifyContent || `flex-start`};
  align-items: ${({ alignItems }: { alignItems: string }) =>
    alignItems || `flex-start`};
  flex-grow: ${({ flexGrow }: { flexGrow: number }) => flexGrow || `0`};
`;

export default Flex;
