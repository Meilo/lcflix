import React from "react";
import { Icon as RawIcon } from "@ui-kitten/components";

interface Props {
  name: string;
}

const Icon = ({ name, ...otherProps }: Props) => (
  <RawIcon {...otherProps} name={name} />
);

export default Icon;
