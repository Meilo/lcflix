import styled from "styled-components";
import themeCustom from "../theme/themeCustom";
import { View } from "react-native";

interface Props {
  size:
    | "tiny"
    | "verySmall"
    | "small"
    | "standard"
    | "large"
    | "veryLarge"
    | "extraLarge";
  left?: string;
  top?: string;
  right?: string;
  bottom?: string;
}

const { padding } = themeCustom;

const Padded = styled(View)`
  padding-left: ${({ left, size }: Props) =>
    left ? `${size ? padding[size] : padding.standard}px` : `0px`};
  padding-top: ${({ top, size }: Props) =>
    top ? `${size ? padding[size] : padding.standard}px` : `0px`};
  padding-right: ${({ right, size }: Props) =>
    right ? `${size ? padding[size] : padding.standard}px` : `0px`};
  padding-bottom: ${({ bottom, size }: Props) =>
    bottom ? `${size ? padding[size] : padding.standard}px` : `0px`};
`;

export default Padded;
