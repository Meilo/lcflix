import "react-native-gesture-handler";
import React, { useState } from "react";
import { StatusBar } from "expo-status-bar";
import { AppearanceProvider, useColorScheme } from "react-native-appearance";
import * as eva from "@eva-design/eva";
import { ApplicationProvider, IconRegistry } from "@ui-kitten/components";
import { EvaIconsPack } from "@ui-kitten/eva-icons";
import AppNavigator from "./src/appNavigator";
import { default as uiKittenTheme } from "./src/theme/theme.json";
import { useWishlist, useMovies } from "./src/api";
import { WishlistProvider } from "./src/context/wishlistContext";
import { MoviesProvider } from "./src/context/moviesContext";

interface ApparenceT {
  theme: ["light" | "dark", (data: "light" | "dark") => void];
}

const App = () => {
  const wishlist = useWishlist();
  const movies = useMovies();
  const colorScheme: any = useColorScheme();
  const [theme, setTheme]: ApparenceT["theme"] = useState(colorScheme);

  return (
    <AppearanceProvider>
      <WishlistProvider value={wishlist}>
        <MoviesProvider value={movies}>
          <IconRegistry icons={EvaIconsPack} />
          <ApplicationProvider
            {...eva}
            theme={{ ...eva[theme], ...uiKittenTheme }}
          >
            <AppNavigator apparence={theme} />
          </ApplicationProvider>
          <StatusBar style={theme === "dark" ? "light" : "dark"} />
        </MoviesProvider>
      </WishlistProvider>
    </AppearanceProvider>
  );
};

export default App;
